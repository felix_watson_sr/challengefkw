package json;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Ingestor {

	public static void main(String[] args) {
		
		if (args.length < 2) {
			System.out.println("Usage: <infile 1> <infile 2> ... <infile n> <outfile>");
			System.out.println("Please provide at least one input file name and only one output file name.");
			return;
		}
		
		ArrayList<Device> list = new ArrayList<Device>(); //total devices
		ArrayList<Device> uniqueList = new ArrayList<Device>(); //unique devices
		
		Statistics stats = new Statistics(0.0, 89.99, -179.99, 0.0);
		for (int i = 0; i < args.length - 1; i++) {
			String filename = args[i];
			BufferedReader reader;
			if (filename.endsWith(".csv")) {
				try {
					reader = new BufferedReader(new FileReader(filename));
					new Ingestor().ingestCSV(reader, list, stats);
//					System.out.println("list size after .cvs "+list.size());
					reader.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (filename.endsWith(".json")) {
				try {
					reader = new BufferedReader(new FileReader(filename));
					new Ingestor().ingestJSON(reader, list, stats);
//					System.out.println("list size after json "+list.size());
					reader.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		for (int i = 0; i < list.size(); i++) {
			Device d = list.get(i);
			boolean alreadyInList = false;
			for (int j = 0; j < uniqueList.size(); j++) {
				Device ud = uniqueList.get(j);
				//uniqueness
				if (Device.equals(d, ud)) {
					alreadyInList = true;
				}
			}
			if (!alreadyInList) {
				uniqueList.add(d);
				stats.incrementUniqueTotal();
				//timestamps
				long ts = d.getTimestamp();
				String id = d.getUUID();
				stats.updateTimestamps(ts, id);
				//geographic area
				stats.updateAreaOfInterestTotal(d);
			}
			
		}
//		System.out.println("Total devices "+stats.getTotal());
//		System.out.println("Unique devices "+stats.getUniqueTotal());
//		System.out.println("Geographic Area of Interest total "+stats.getAreaOfInterestTotal());
//		System.out.println("oldestTimestamp "+stats.getOldestTimestamp()
//		+" "+ Statistics.convertTimestampToDateAndTime(stats.getOldestTimestamp()));
//		System.out.println("newestTimestamp "+stats.getNewestTimestamp()
//		+" "+ Statistics.convertTimestampToDateAndTime(stats.getNewestTimestamp()));
		try {
			FileWriter outputFile = new FileWriter(new File(args[args.length - 1]));
			stats.writeJSON(outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void ingestCSV(BufferedReader reader, ArrayList<Device> list, Statistics stats) {
		String line, headerLine;
		String[] header;
		try {
			headerLine = reader.readLine();
			if (headerLine == null) {
				return;
			}
			header = headerLine.split(",");
			if (header.length != 4 ||
					!header[0].equals(Device.KEYS[0]) ||
					!header[1].equals(Device.KEYS[1]) ||
					!header[2].equals(Device.KEYS[2]) ||
					!header[3].equals(Device.KEYS[3])) {
				System.out.println("Invalid input file format!");
				return;
			}
			line = reader.readLine();
			while(!(line == null)) {
				String id;
				double lat;
				double lon;
				long timestamp;
				String[] vals = line.split(",");
				id = vals[0];
				try {
					lat = Double.parseDouble(vals[1]);
					lon = Double.parseDouble(vals[2]);
					timestamp = Long.parseLong(vals[3]);
					Device d = new Device(id, lat, lon, timestamp);
					list.add(d);
					stats.incrementTotal();
				} catch (NumberFormatException e) {
					System.out.println("invalid device "+vals);
				}
				line=reader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void ingestJSON(BufferedReader reader, ArrayList<Device> list, Statistics stats) {
		String headerLine, line;
		String[] vals = null;
		String id;
		double lat;
		double lon;
		long timestamp;
		try {
			headerLine = reader.readLine().trim();
			if (headerLine == null) {
				return;
			}
			if (!headerLine.equals("{\"devices\": [")) {
				System.out.println("Invalid input file format!");
				return;
			}
			while(true) {
				try {
					line = reader.readLine();//open
					if (line==null) {
						break;
					}
					line = reader.readLine();//uuid
					if (line == null) {
						break;
					}
					vals = line.split(":");
					vals = vals[1].split("\"");
					id = vals[1];
					
					line = reader.readLine();//lat
					if (line == null) {
						break;
					}
					vals = line.split(":");
					vals = vals[1].split(",");
					lat = Double.parseDouble(vals[0]);
					
					line = reader.readLine();//long
					if (line == null) {
						break;
					}
					vals = line.split(":");
					vals = vals[1].split(",");
					lon = Double.parseDouble(vals[0].trim());
					
					line = reader.readLine();//timestamp
					if (line == null) {
						break;
					}
					vals = line.split(":");
					timestamp = Long.parseLong(vals[1].trim());
					
					Device d = new Device(id, lat, lon, timestamp);
					list.add(d);
					stats.incrementTotal();
					line = reader.readLine(); //close
					if (line == null) {
						break;
					}
					
				} catch (NumberFormatException e) {
					System.out.println("invalid device "+vals);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
