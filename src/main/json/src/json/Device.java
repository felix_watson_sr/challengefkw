package json;

public class Device {
	public static final String[] KEYS = {"uuid", "lat", "long","timestamp"};
	private String uuid;
	private double lattitude;
	private double longitude;
	private long timestamp;
	
	public static boolean equals(Device a, Device b) {
		if (a.lattitude == b.lattitude && 
				a.longitude == b.longitude &&
				a.timestamp == b.timestamp) {
			return true;
		}
		return false;
	}
	
	public Device(String uuid, double lattitude, double longitude, long timestamp) {
		super();
		this.uuid = uuid;
		this.lattitude = lattitude;
		this.longitude = longitude;
		this.timestamp = timestamp;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public String getUUID() {
		return new String(uuid);
	}
	
	public double getLat() {
		return lattitude;
	}
	
	public double getLon() {
		return longitude;
	}
	
}
