package json;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Statistics {
	private int total = 0;
	private int uniqueTotal = 0;
	private int areaOfInterestTotal = 0;
	private long oldestTimestamp = Long.MAX_VALUE;
	private long newestTimestamp = Long.MIN_VALUE;
	private double minLat;
	private double maxLat;
	private double minLon;
	private double maxLon;
	private String oldestDeviceID = null;
	private String newestDeviceID = null;
	
	public Statistics(double minLat, double maxLat, double minLon, double maxLon) {
		super();
		this.minLat = minLat;
		this.maxLat = maxLat;
		this.minLon = minLon;
		this.maxLon = maxLon;
	}
	
	public void incrementTotal() {
		total++;
	}
	
	public int getTotal() {
		return total;
	}
	
	public void incrementUniqueTotal() {
		uniqueTotal++;
	}
	
	public int getUniqueTotal() {
		return uniqueTotal;
	}
	
	public void updateTimestamps(long timestamp, String id) {
		if (oldestTimestamp > timestamp) {
			oldestTimestamp = timestamp;
			oldestDeviceID  = new String(id);
		}
		if (newestTimestamp < timestamp) {
			newestTimestamp = timestamp;
			newestDeviceID  = new String(id);
		}
	}
	
	public long getOldestTimestamp() {
		return oldestTimestamp;
	}
	
	public long getNewestTimestamp() {
		return newestTimestamp;
	}
	
	public void updateAreaOfInterestTotal(Device d) {
		double lat = d.getLat();
		double lon = d.getLon();
		if (lat >= minLat && lat <= maxLat && lon >= minLon && lon <= maxLon) {
			areaOfInterestTotal++;
		}
	}
	
	public int getAreaOfInterestTotal() {
		return areaOfInterestTotal;
	}
	
	public static String convertTimestampToDateAndTime(long timestamp) {
	    Date d = new Date(timestamp);
	    Calendar c = new GregorianCalendar();
	    SimpleDateFormat dateFormatter = 
	    		new SimpleDateFormat("MMM dd yyyy hh:mm:ss:SS z");
	    dateFormatter.setCalendar(c);
	    c.setTime(d);
	    String dateAndTime = dateFormatter.format(d);
	    return dateAndTime;
	}
	
	public void writeJSON(FileWriter fw) throws IOException {
		fw.write("{\"statistics\" : \n");
		fw.write("\t{\n");
		fw.write("\t\t\"total\": "+total+",\n");
		fw.write("\t\t\"uniquetotal\" : "+uniqueTotal+",\n");
		fw.write("\t\t\"areaofinteresttotal\" : "+areaOfInterestTotal+",\n");
		fw.write("\t\t\""+oldestDeviceID+"\" : \""+convertTimestampToDateAndTime(oldestTimestamp)+"\",\n");
		fw.write("\t\t\""+newestDeviceID+"\" : \""+convertTimestampToDateAndTime(newestTimestamp)+"\"\n");
		fw.write("\t}\n");
		fw.write("}\n");
		fw.flush();
		fw.close();
	}
}
